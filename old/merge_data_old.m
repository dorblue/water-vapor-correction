function [mergedSpData] = merge_data_old(mainSpData, sizeArray)

numbOfSpectra = sum(sizeArray(:, 1));
numbOfWns = max(sizeArray(:, 2));

mergedSpData = struct('d', {}, 'i', {}, 'v', {});
mergedSpData(1).d = zeros(numbOfSpectra, numbOfWns);
mergedSpData(1).v = mainSpData{1}.v;
% 16 is chosen as the maximum value for the parameter
mergedSpData(1).i = char(zeros(numbOfSpectra, 16));

sumOfSpectra = 0;
for i = 1: length(mainSpData)
    mergedSpData.d((sumOfSpectra + 1): (sumOfSpectra + ...
        sizeArray(i, 1)), :) = mainSpData{i}.d;
    mergedSpData.i((sumOfSpectra + 1): (sumOfSpectra + ...
        sizeArray(i, 1)), 1: length(mainSpData{i}.i(1, :))) = ...
        mainSpData{i}.i;
    sumOfSpectra = sumOfSpectra + sizeArray(i, 1);
end