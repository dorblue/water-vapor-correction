function [mergedSpData] = merge_opus_data(mainSpData)

mergedSpData = struct('d', {}, 'i', {}, 'v', {});

%% Filling in the spectral data component of mergedSpData struct
[numbOfSp_1, numbOfWns] = size(mainSpData{1}.d);
[numbOfSp_2, ~] = size(mainSpData{2}.d);
[numbOfSp_3, ~] = size(mainSpData{3}.d);
mergedSpData(1).d = zeros(numbOfSp_1 + numbOfSp_2 + numbOfSp_3, numbOfWns);
mergedSpData.d(1: numbOfSp_1, :) = mainSpData{1}.d;
mergedSpData.d((numbOfSp_1 + 1): (numbOfSp_1 + numbOfSp_2), ...
    :) = mainSpData{2}.d;
mergedSpData.d((numbOfSp_1 + numbOfSp_2 + 1): (numbOfSp_1 + ... 
    numbOfSp_2 + numbOfSp_3), :) = mainSpData{3}.d;

%% Filling in the wavenumber component of mergedSpData struct
mergedSpData.v = mainSpData{1}.v;

%% Filling in the grouping description component of mergedSpData struct
grDescrLen = max(length(mainSpData{1}.i(1, :)), ...
    length(mainSpData{2}.i(1, :)));
grDescrLen = max(grDescrLen, length(mainSpData{3}.i(1, :)));
mergedSpData.i = char(zeros(numbOfSp_1 + numbOfSp_2 + ...
    numbOfSp_3, grDescrLen));
mergedSpData.i(1: numbOfSp_1, 1: ...
    length(mainSpData{1}.i(1, :))) = mainSpData{1}.i;
mergedSpData.i((numbOfSp_1 + 1) : (numbOfSp_1 + numbOfSp_2), ...
    1: length(mainSpData{2}.i(1, :))) = mainSpData{2}.i;
mergedSpData.i((numbOfSp_1 + numbOfSp_2 + 1) : (numbOfSp_1 + ...
    numbOfSp_2 + numbOfSp_3), 1: length(mainSpData{3}.i(1, :))) = mainSpData{3}.i;

end