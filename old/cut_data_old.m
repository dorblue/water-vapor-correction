function [cutData] = cut_data(data, array)

numbOfSpectra = length(array);

cutData = struct();
cutData(1).d = zeros(numbOfSpectra, length(data.v));
cutData(1).i = char(zeros(numbOfSpectra, length(data.i(1, :))));
cutData(1).v = data.v;

for i = 1: numbOfSpectra
    currIndex = array(i);
    cutData.d(i, :) = data.d(currIndex, :);
    cutData.i(i, :) = data.i(currIndex, :);
end

disp ("BP");